import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';
// import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
  })
export class JWTTokenService {

    jwtToken: string ="";
    decodedToken?: { [key: string]: string };

    constructor() {
    }

    setToken(token: string) {
      if (token) {
        this.jwtToken = token;
      }
    }

    decodeToken() {
      if (this.jwtToken) {
      this.decodedToken = jwtDecode(this.jwtToken);
      }
    }

    getDecodeToken() {
      return jwtDecode(this.jwtToken);
    }

    getUser() {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.displayname : null;
    }

    getEmailId() {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.email : null;
    }

    getExpiryTime():string {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.exp : "";
    }
    getEntornoImagen():string {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.entornoImagen : "";
    }
    getEntorno():string {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.entorno : "";
    }
    getEntornoNombre():string {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.entornoNombre : "";
    }
    getSub():string {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.sub : "";
    }
    isTokenExpired(): boolean {
      /*
      console.log(1000 * Number(this.getExpiryTime()) )
      console.log((new Date()).getTime())
      console.log((1000 * Number(this.getExpiryTime())) - (new Date()).getTime())
      */
      if (this.getExpiryTime()) {
        return ((1000 * Number(this.getExpiryTime())) - (new Date()).getTime()) < 5000;
      } else {
        return false;
      }
    }
}

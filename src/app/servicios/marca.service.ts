import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { Marcas } from '../modelos/marcas';
import { TOKEN_KEY } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {


  constructor(private _http : HttpClient, private _url : UrlService) { }

  getAllMarca() : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Marcas[]>(this._url.getURLBase() + 'marca',{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

}



import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { VehiculosService } from './vehiculos.service';
import { UrlService } from './url.service';

import { Vehiculos } from '../modelos/vehiculos';
import { OrdenServicio } from '../modelos/ordenesServicio';
import { TOKEN_KEY } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class OrdenServicioService {
  private vehiculo : Vehiculos;

  constructor(private _http : HttpClient,
              private _url : UrlService,
              private _servicioVehiculo : VehiculosService,
              private url : UrlService) { }

  getAllOrdenes(pagina : number = 0) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<OrdenServicio[]>(this._url.getURLBase() + 'ordendeservicio?page=' + pagina + '&size=8',{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  getAllOrdenesByDominio(dominio : string, pagina : number = 0) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<OrdenServicio[]>(this._url.getURLBase() + 'ordendeservicio/listar/' + dominio + '?page=' + pagina + '&size=8',{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  getOrden(codigo) : Observable<any>{
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<OrdenServicio>(this._url.getURLBase() + 'ordendeservicio/'+codigo,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  obtenerVehiculos(dominio : string) {
    this._servicioVehiculo.getVehiculos(dominio).subscribe(respuesta => {
      this.vehiculo = respuesta.content[0];
      console.log(this.vehiculo);
    },error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    });
  }

  getVehiculo() {
    return this.vehiculo;
  }

  guardarOrden(orden : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.post(this._url.getURLBase() + 'ordendeservicio',orden,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  modificarOrden(orden : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.put(this._url.getURLBase() + 'ordendeservicio/'+orden.codigo,orden,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  anularOrden(codigo : number) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.delete(this._url.getURLBase() + 'ordendeservicio/'+codigo,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  abonarOrden(orden : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.put(this._url.getURLBase() + 'ordendeservicio/abonada',orden,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  cerrarOrden(orden : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.put(this._url.getURLBase() + 'ordendeservicio/cerrada',orden,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  downloadFile(codigo : number) {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Blob>(this._url.getURLBase() + 'reportes/ordendeservicio/' +codigo, {headers: new HttpHeaders().set('Authorization',  `${jwt}`), observe: 'response', responseType: 'blob' as 'json' })
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { Vehiculos } from '../modelos/vehiculos';
import { TOKEN_KEY } from '../app.component';


@Injectable()
export class VehiculosService {

  constructor(private _http : HttpClient,
              private _url : UrlService) {}
  
  //Busca todos los Vehiculos al carga la ventana.
  getAllVehiculos(pagina : number = 0) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Vehiculos[]>(this._url.getURLBase() + 'vehiculo?page=' + pagina + '&size=7',{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  //Busca varios Vehiculos, a partir del filtro: dominio
  getVehiculos(txtBuscar : string) : Observable<any> {
    //return this._http.get<Vehiculos[]>(this._url.getURLBase() + 'vehiculo?dominio=' + txtBuscar);
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Vehiculos[]>(this._url.getURLBase() + 'vehiculo?dominio=' + txtBuscar,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  //Busca un vehículo exacto a partir de un dominio
  consultarVehiculo(txtBuscar : string) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Vehiculos>(this._url.getURLBase() + 'vehiculo/' + txtBuscar,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  guardarVehiculo(vehiculo : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.post(this._url.getURLBase() + 'vehiculo',vehiculo,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  modificarVehiculo(vehiculo : any) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.put(this._url.getURLBase() + 'vehiculo/' + vehiculo.id, vehiculo,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  eliminarVehiculo(id : number) : Observable<any> {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.delete(this._url.getURLBase() + 'vehiculo/' + id,{headers: new HttpHeaders().set('Authorization',  `${jwt}`)});
  }

  downloadFile() {
    let jwt:any = window.sessionStorage.getItem(TOKEN_KEY);
    return this._http.get<Blob>(this._url.getURLBase() + 'reportes/vehiculos', {headers: new HttpHeaders().set('Authorization',  `${jwt}`), observe: 'response', responseType: 'blob' as 'json' })
  }



}

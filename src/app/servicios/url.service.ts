import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  private URL : string = window["cfgApiBaseUrl"] + "/";//web";// En index dejé un "/" para que funcione, hasta tanto no exporte al Tomcat.
  private URLAuth : string = window["cfgApiBaseUrlAuth"];
  constructor() { }

  getURLBase() : string {
    return this.URL;
  }
  
  getURLAth() : string {
    return this.URLAuth;
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './componentes/home/home.component';
import { VehiculosComponent } from './componentes/vehiculos/vehiculos.component';
import { VehiculoComponent } from './componentes/vehiculo/vehiculo.component';
import { NuevoVehiculoComponent } from './componentes/nuevo-vehiculo/nuevo-vehiculo.component';
import { ModificarVehiculoComponent } from './componentes/modificar-vehiculo/modificar-vehiculo.component';
import { OrdenesServiciosComponent } from './componentes/ordenes-servicios/ordenes-servicios.component';
import { NuevaOrdenServicioComponent } from './componentes/nueva-orden-servicio/nueva-orden-servicio.component';
import { ModificarOrdenServicioComponent } from './componentes/modificar-orden-servicio/modificar-orden-servicio.component';
import { OrdenServicioComponent } from './componentes/orden-servicio/orden-servicio.component';
import { PresupuestosComponent } from './componentes/presupuestos/presupuestos.component';
import { NuevoPresupuestoComponent } from './componentes/nuevo-presupuesto/nuevo-presupuesto.component';
import { ModificarPresupuestoComponent } from './componentes/modificar-presupuesto/modificar-presupuesto.component';
import { PresupuestoComponent } from './componentes/presupuesto/presupuesto.component';
import { OsPorVehiculoComponent } from './componentes/os-por-vehiculo/os-por-vehiculo.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent,},
  {path: 'vehiculos', component: VehiculosComponent,},
  {path: 'vehiculos/new', component: NuevoVehiculoComponent},
  {path: 'vehiculos/update/:dominio', component: ModificarVehiculoComponent},
  {path: 'vehiculos/:dominio', component: VehiculoComponent},
  {path: 'ordenes-servicios', component: OrdenesServiciosComponent},
  {path: 'ordenes-servicios/new', component: NuevaOrdenServicioComponent},
  {path: 'ordenes-servicios/update/:nro-orden', component: ModificarOrdenServicioComponent},
  {path: 'ordenes-servicios/:nro-orden', component: OrdenServicioComponent},
  {path: 'ordenes-servicios/listar/:dominio', component: OsPorVehiculoComponent},
  // {path: 'presupuestos', component: PresupuestosComponent},
  // {path: 'presupuestos/new', component: NuevoPresupuestoComponent},
  // {path: 'presupuestos/update/:nro-presupuesto', component: ModificarPresupuestoComponent},
  // {path: 'presupuestos/:nro-presupuesto', component: PresupuestoComponent},
  {path: '**', pathMatch:'full', redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

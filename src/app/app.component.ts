import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JWTTokenService } from 'src/JWTTokenService';
import { UrlService } from './servicios/url.service';

export const TOKEN_KEY = 'auth-token';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'apptm';
  static app:AppComponent;
  constructor(private router: Router,private _router : Router,private route: ActivatedRoute,
    private url : UrlService, private jwtService:JWTTokenService,private modalService: NgbModal){
    
    this.controlSesion()
    router.events.subscribe((val) => {
      this.controlSesion()
    });
    AppComponent.app = this;
    this.tieneAuth = this.url.getURLAth()!=null
  }
  ingresoToken :boolean = false
  tieneAuth
  jwt
  controlSesion(){
    let token = this.route.snapshot.queryParams.token

    if(token){
      window.sessionStorage.setItem(TOKEN_KEY, token);
      this._router.navigate(['/']);
      this.ingresoToken = true;
    }else if(this.url.getURLAth() && !window.sessionStorage.getItem(TOKEN_KEY) && this.ingresoToken){
      try{
        let jwt = window.sessionStorage.getItem(TOKEN_KEY)
        if(jwt){
          this.jwtService.setToken(jwt)
          if(this.jwtService.isTokenExpired()){
            window.sessionStorage.setItem(TOKEN_KEY,null)
            window.location.href = this.url.getURLAth();
          }
        }else{
          window.location.href = this.url.getURLAth();
        }
      }catch (e) {
        window.location.href = this.url.getURLAth();
      }
    }
    this.jwt = window.sessionStorage.getItem(TOKEN_KEY)
  }

  confirmacion(){
    AppComponent.confirmacion({
      titulo:"titulo desde aca papa", 
      mensaje:"mensaje alguno lo que quieras",
      onAceptar:()=>{console.log("----Aceptar---")},
      onCancelar:()=>{console.log("---Cancelar---")}
    });
    }



    @ViewChild('content') content;
    data
    public static confirmacion(data: any){
      AppComponent.app.open(AppComponent.app.content, data)
      AppComponent.app.data = data;
    }
    closeResult = '';
    open(content:any, data:Object) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(
      (result) => {
        if(result=='ACEPTAR'){
          if(AppComponent.app.data.onAceptar)
            AppComponent.app.data.onAceptar();
        }else{
          if(AppComponent.app.data.onCancelar)
            AppComponent.app.data.onCancelar();
        }
      }, (reason) => {
        if(AppComponent.app.data.onCancelar)
          AppComponent.app.data.onCancelar();
      });
    }

}


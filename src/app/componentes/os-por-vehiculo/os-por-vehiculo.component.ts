import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrdenServicio } from 'src/app/modelos/ordenesServicio';
import { OrdenServicioService } from 'src/app/servicios/orden-servicio.service';
import { UrlService } from 'src/app/servicios/url.service';

@Component({
  selector: 'app-os-por-vehiculo',
  templateUrl: './os-por-vehiculo.component.html',
  styleUrls: ['./os-por-vehiculo.component.css']
})
export class OsPorVehiculoComponent implements OnInit {

  ordenes : OrdenServicio[] = [];
  txtBuscar : string = '';
  filtrar : string = '';
  filtrarDominio : string = '';
  cantidadPaginas : number;
  paginaActual : number;
  dominio : string;
  tieneOS = false;

  constructor(private _servicioOrdenes : OrdenServicioService,
              private _formatFecha     : DatePipe,
              private _router          : Router,
              private _activate        : ActivatedRoute,
              private url : UrlService) {
      this.listarOSPorDominio();
  }

  ngOnInit(): void {
    if(localStorage.getItem("vehiculo")){localStorage.removeItem("vehiculo");}
  }

  setOrdenes(data) : void {
    this.ordenes = data;
  }

  getOrdenes() : OrdenServicio[] {
    return this.ordenes;
  }

  getPropietario(os: OrdenServicio) : string {
    let propietario : string = "";
    if(os.razonSocialTitular){
      propietario = os.razonSocialTitular;
    } else if ((os.nombreTitular) && (os.apellidoTitular)) {
      propietario = os.apellidoTitular + ', ' + os.nombreTitular;
    } else {
      propietario = "Se ha producido un error intentando recuperar el propietario."
    }
    return propietario;
  }

  buscarOrden(){
    if (this.txtBuscar != ''){
      this._servicioOrdenes.getOrden(this.txtBuscar).subscribe(respuesta =>{
        this.setOrdenes(respuesta.content);
      },
      error => {
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Hubo un inconveniente al intentar obtener la Orden de Servicio N° "+this.txtBuscar+". "+ error
        });
      });
      this.txtBuscar='';
    }
  }

  getFecha(fecha : any) : string {
    return this.setDate(fecha);
  }

  setDate(fecha : any ) : string {
    let ahora : number = fecha;
    return this._formatFecha.transform(ahora,'dd/MM/yyyy');
  }

  limpiarBusqueda() {
    this.txtBuscar = '';
    this.listarOSPorDominio();
  }

  listarOSPorDominio() {
    this.dominio=this._activate.snapshot.params['dominio'];
    this._servicioOrdenes.getAllOrdenesByDominio(this.dominio).subscribe(respuesta => {
      this.setOrdenes(respuesta.content);
      if (this.getOrdenes().length > 0) {
        this.tieneOS = true;
      }
      this.cantidadPaginas = respuesta.totalPages
      this.paginaActual = respuesta.pageable.pageNumber;
      },
      error => {
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Hubo un inconveniente al intentar obtener las Órdenes de Servicio. "+ error
        });
        console.log(error.message);
      });
  }

  verDetallesOrdenes(codigo) {
    this._router.navigate(['/ordenes-servicios', codigo]);
  }

  noAnulable(estado : string) : boolean{
    if(((estado).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  anularOrden(codigo : number, estado : string) : void {
    if(!this.noAnulable(estado)){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Anulada?",
        onAceptar:()=>{
        	this.confirmarAnularOrden(codigo,estado)
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarAnularOrden(codigo : number, estado : string) : void {
        this._servicioOrdenes.anularOrden(codigo).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          this.listarOSPorDominio();
        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Anular la Orden de Servicio con código N° '+codigo+'. '+error
          });
        });
      }


  noAbonable(estado : string) : boolean{
    if(((estado).indexOf("Abonada"))>=0 || ((estado).indexOf("Anulada"))>=0) {
      return true;
    } else {
      return false;
    }
  }

  abonarOrden(codigo : number, estado : string){
    //if(!this.noAbonable(estado)){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Abonada?",
        onAceptar:()=>{
        	this.confirmarAbonarOrden(codigo,estado)
        },
        onCancelar:()=>{}
      });
  }
  confirmarAbonarOrden(codigo : number, estado : string){
        this._servicioOrdenes.abonarOrden(this.obtenerOrden(codigo)).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          this.listarOSPorDominio();
        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Abonar la Orden de Servicio con código N° '+codigo+'. '+error
          });
          console.log(error.message);
        });
      }


  noCerrable(estado : string) : boolean{
    if(((estado).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  cerrarOrden(codigo : number, estado : string){
    if(!this.noCerrable(estado)){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Cerrada?",
        onAceptar:()=>{
        	this.confirmarCerrarOrden(codigo,estado)
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarCerrarOrden(codigo : number, estado : string){

        this._servicioOrdenes.cerrarOrden(this.obtenerOrden(codigo)).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          this.listarOSPorDominio();
        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Cerrar la Orden de Servicio con código N° '+codigo+'. '+error
          });
        });
      }


  obtenerOrden(codigo : number) : OrdenServicio {
    let os : OrdenServicio = new OrdenServicio;
    this.getOrdenes().forEach(element => {
      if(codigo == element.codigo){
        os = element;
      }
    });
    return os;
  }

  getFileName(response: HttpResponse<Blob>, codigo : number) {
    let filename: string;
    try {
      const contentDisposition: string = response.headers.get('content-disposition');
      const r = /(?:filename=")(.+)(?:")/
      filename = r.exec(contentDisposition)[1];
    }
    catch (e) {
      let nroOrden : number = codigo;
      filename = 'Orden' + nroOrden + '.pdf';
    }
    return filename
  }

  descargarOS(codigo: number) {
    this._servicioOrdenes.downloadFile(codigo)
      .subscribe(
        (response: HttpResponse<Blob>) => {
          let filename: string = this.getFileName(response, codigo)
          let binaryData = [];
          binaryData.push(response.body);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: 'blob' }));
          downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },error=>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
        }
      )
  }

  pasarPagina(valor : number) {
    this.paginaActual+=valor;
    this._servicioOrdenes.getAllOrdenes(this.paginaActual).subscribe(respuesta => {
      this.setOrdenes(respuesta.content);
    },error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    })
  }

  irAPagina(valor : number){
    this._servicioOrdenes.getAllOrdenes(valor).subscribe(respuesta => {
      this.setOrdenes(respuesta.content);
    },error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    })
    this.paginaActual=valor;
  }

  nuevaOrdenServicio() {
    this._router.navigate(['/ordenes-servicios/new']);
  }

}

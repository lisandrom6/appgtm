import { Component, OnInit } from '@angular/core';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { Marcas } from 'src/app/modelos/marcas';
import { setClassMetadata } from '@angular/core/src/r3_symbols';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {

  dominio : string = '';
  vehiculo : Vehiculos = new Vehiculos();
  txtBuscar : string = '';

  constructor(private _servicioVehiculo : VehiculosService,
              private _router           : Router,
              private _activate         : ActivatedRoute,
              private url : UrlService) { }

  ngOnInit(): void {
    this.reload();
  }

  reload() {

    // CÓDIGO DE CARGA
    this.dominio=this._activate.snapshot.params['dominio'];
    this._servicioVehiculo.getVehiculos(this.dominio).subscribe(respuesta =>{
      this.setVehiculo(respuesta.content[0]);
      this.localstorage(respuesta.content[0].dominio)
    }, error => {
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
      
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"No recibo respuesta del BACK" + error
      });
    })
    // // CÓDIGO DE CARGA
    // this.dominio=this._activate.snapshot.params['dominio'];
    // if (this.dominio == undefined){
    //   console.log('Ingresó.')
    //   this.buscarVehiculo(this.dominio);
    // }
  }

  buscarVehiculo(dominio : string = this.txtBuscar){
    if (this.validarInputVacio(dominio) == true) {

      this._servicioVehiculo.consultarVehiculo(dominio).subscribe(respuesta =>{
        if (respuesta.content[0] == null) {
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:"No se encontraron resultados para el valor ingresado. Intente ingresando otro valor, registre un nuevo vehículo o diríjase a la sección Vehículos"
          });
          this.limpiarBusqueda();
        } else {
          this.setVehiculo(respuesta.content[0]);
        }
        this.localstorage(respuesta.content[0].dominio)

      }, error => {
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"No recibo respuesta del BACK" + error
        });
      })
    };

  }

  localstorage(respuesta){
    localStorage.removeItem("vehiculo");
    localStorage.setItem("vehiculo", respuesta);
    console.log(localStorage.getItem("vehiculo"));
  }
  setVehiculo(dato){
    this.vehiculo = dato;
  }

  getVehiculo(){
    return this.vehiculo;
  }

  modificarVehiculo(dominio : string) {
    localStorage.removeItem("vehiculo");
    this._router.navigate(['/vehiculos/update/', dominio]);
  }

  eliminarVehiculo() {
    AppComponent.confirmacion({
      titulo:"Confirmación", 
      mensaje:"Está seguro que desea eliminar el Vehículo: " + this.getVehiculo().dominio + "?",
      onAceptar:()=>{
        this.confirmarEliminarVehiculo();
      },
      onCancelar:()=>{}
    });
  }
  confirmarEliminarVehiculo(){
      this._servicioVehiculo.eliminarVehiculo(this.getVehiculo().id).subscribe(respuesta => {
        let listaMsjs = "";
        for (let msj of respuesta.mensajes) {
          listaMsjs+=(msj.descripcion+"<br>");
        }

        AppComponent.confirmacion({
          titulo:"Resultado", 
          mensaje: listaMsjs
        });

      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'No se ha podido Eliminar el Vehículo: ' + this.getVehiculo().dominio + ' .' + error
        });
      });
    }
  

  limpiarBusqueda() {
    localStorage.removeItem("vehiculo");
    this.setVehiculo(undefined);
    this.txtBuscar = '';
  }

  validarInputVacio(input : string) {
    let value = input.trim().length;
    if (value > 0) {
      return true;
    } else {
      AppComponent.confirmacion({
        titulo:"Información", 
        mensaje:"Campo vacío. Ingrese un valor para continuar",
        onAceptar:()=>{}
      });
      return false;
    };
  }

  verOSPorVehiculo(dominio) {
    this._router.navigate(['/ordenes-servicios/listar', dominio]);
  }

  nuevaOrdenServicio() {
    localStorage.removeItem("vehiculo");
    localStorage.setItem("vehiculo", this.vehiculo.dominio);
    console.log(localStorage.getItem("vehiculo"));
    this._router.navigate(['/ordenes-servicios/new']);
    }

}

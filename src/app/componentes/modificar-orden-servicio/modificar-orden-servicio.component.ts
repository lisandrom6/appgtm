import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { DetalleOrden } from 'src/app/modelos/detallesOrden';
import { Marcas } from 'src/app/modelos/marcas';
import { OrdenServicio } from 'src/app/modelos/ordenesServicio';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { OrdenServicioService } from 'src/app/servicios/orden-servicio.service';
import { UrlService } from 'src/app/servicios/url.service';

@Component({
  selector: 'app-modificar-orden-servicio',
  templateUrl: './modificar-orden-servicio.component.html',
  styleUrls: ['./modificar-orden-servicio.component.css']
})
export class ModificarOrdenServicioComponent implements OnInit {

  ordenServicio : OrdenServicio = new OrdenServicio();
  txtBuscar : string ="";
  enviado : boolean = false;

  constructor(private _servicioOrdenes : OrdenServicioService, 
              private _activate        : ActivatedRoute,
              private _router          : Router,
              private _formatFecha     : DatePipe,
              private url : UrlService ) {
    this.ordenServicio.vehiculo = new Vehiculos();
    this.ordenServicio.vehiculo.marca = new Marcas();
    this._activate.params.subscribe((parametros) =>{
      this._servicioOrdenes.getOrden(parametros['nro-orden']).subscribe(respuesta =>{
        this.setOrdenServicio(respuesta.content[0]);
      },
      error=>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'No se ha encotnrado la Orden de Servicio con código N° '+parametros['nro-orden']+'. '+error,
        });
      });
    });
  }

  ngOnInit(): void {
  }

  setOrdenServicio(orden) : any {
    this.ordenServicio = orden;
    this.setPropietario();
    this.setNivelCombustible();
    this.setAbonada();
  }

  setAbonada() {
    let checkbox = document.querySelector('#abonada');
    checkbox.removeAttribute('checked');
    let bool : string;
    if(this.getOrdenServicio().abonada){
      bool="true";
      checkbox.setAttribute('checked',bool);
    }
  }
 
  getFecha() : string {
    return this.setDate(this.getOrdenServicio().fecha)
  }

  setDate(fecha : any  = Date.now()) : string {
    let ahora : number = fecha;
    return this._formatFecha.transform(ahora,'dd/MM/yyyy');
  }

  checkCheckBoxvalue(event){
    this.getOrdenServicio().abonada = !(this.getOrdenServicio().abonada);
  }

  setNivelCombustible() : void {
    var radio;
    switch (this.getOrdenServicio().nivelCombustible) {
      case 0: radio = document.querySelector('#vacio');
        break;
      case 0.25: radio = document.querySelector('#cuarto');
        break;
      case 0.5: radio = document.querySelector('#medio');
        break;
      case 0.75: radio = document.querySelector('#tres-cuartos');
        break;
      case 1: radio = document.querySelector('#lleno');
        break;
      default: 
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"El nivel de combustible debe ser cargado",
      });
    }
    radio.setAttribute('checked','true');
  }

  getOrdenServicio() : OrdenServicio {
    return this.ordenServicio;
  }

  setEnviado() : void {
    this.enviado = false;
  }

  buscarOrden(){
    if(this.txtBuscar){
      this._servicioOrdenes.getOrden(this.txtBuscar).subscribe(respuesta =>{
        this.setOrdenServicio(respuesta.content[0]);
      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'No se hay conexión con el servidor. '+error,
        });
      });
    } else {
      
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"Debe ingresar un Código de Orden de Servicio para poder visualizarlo.",
      });
    }
  }

  verNuevaOrden(){
    this._router.navigate(['/ordenes-servicios/new']);
  }

  @ViewChild('form') f : NgForm;
  
  limpiarFormulario(form : NgForm = this.f) : void{
    form.reset();
  }
  
  setPropietario() : void {
    let fieldset;           //Las dos variables se crean sólo por legibilidad del código, para no tener que validar en el HTML qué objetos son.
    let input;
    if(!((this.getOrdenServicio().razonSocialTitular == null) && (this.getOrdenServicio().nombreTitular == null))) {
      if ((this.getOrdenServicio().razonSocialTitular == null) || (this.getOrdenServicio().razonSocialTitular.length <= 0 )){
        fieldset = document.querySelector("#content__datosPropietario-fieldset-nombreA").removeAttribute('hidden');
        input = document.querySelector("#nombrePropietario").setAttribute('required','true');

      } else if ((this.getOrdenServicio().nombreTitular == null)||(this.getOrdenServicio().nombreTitular.length <= 0 )){
        fieldset = document.querySelector("#content__datosPropietario-fieldset-razonS").removeAttribute('hidden');
        input = document.querySelector("#razonSocialPropietario").setAttribute('required','true');

      } else {
        
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"No se puede cargar el propietario del vehículo en cuestión. /n Contáctese con su proveedor del sistema. /n Lamentamos las molestias ocasionadas.",
        });
        
      }
    } else {
      
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"No se puede cargar el propietario del vehículo en cuestión. /n Contáctese con su proveedor del sistema. /n Lamentamos las molestias ocasionadas."
      });

    }
  }
  
  setTotal(){
    let total : number = 0;
    this.getOrdenServicio().detalle.forEach(element => {
      total += element.subtotal;
    });
    this.getOrdenServicio().total = total;
  }

  agregarTrabajo(){
    let max : number = 0;
    this.getOrdenServicio().detalle.forEach(element => {
      if(element.id>max){
      max=element.id;
    }});
    let det : DetalleOrden = {id:max+1,descripcion:'',subtotal:0, nueva:true};
    this.getOrdenServicio().detalle.push(det);
  }

  quitarTrabajo(indice : number) :void {
    if (!(this.getOrdenServicio().detalle[indice].subtotal == null || this.getOrdenServicio().total<=0)) {
      this.getOrdenServicio().total -= this.getOrdenServicio().detalle[indice].subtotal; 
    }
    this.getOrdenServicio().detalle.splice(indice,1);
  }

  guardarOrden(f: NgForm){
    if (f.valid) {
      this.seteoFechaRecepcion();
      this.setDetalles();
      this._servicioOrdenes.modificarOrden(this.getOrdenServicio()).subscribe(respuesta => {



        let listaMsjs = "";
        for (let msj of respuesta.mensajes) {
          listaMsjs+=(msj.descripcion+"<br>");
        }

        AppComponent.confirmacion({
          titulo:"Resultado", 
          mensaje: listaMsjs
        });
          
        if(respuesta.objeto != null){
          this.setOrdenServicio(respuesta.objeto);
        }
        
        
        //Validar si se registró OK, mostrar form Registro Exitoso.
        if (respuesta.estado == "OK") {
          this.enviado = true;
          this.limpiarFormulario(f);
        }
      },
        error => {
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:"Ha ocurrido un ERROR en la Comunicación: " + error
          });
      });  
    } else {
      console.log(f.invalid);
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:'Hay campos incompletos o erróneos. Rellene todos los campos con los datos correctos.'
      });
      
    }
  }
  setDetalles() {
    this.getOrdenServicio().detalle.forEach(element => {
      if(element.nueva){
        element.id=0;
    }});
  }

  seteoFechaRecepcion() {
    if(this.getOrdenServicio().fechaRecepcion){
      this.getOrdenServicio().fechaRecepcion = this._formatFecha.transform(this.getOrdenServicio().fechaRecepcion,'yyyy-MM-dd');
    }
  }

  noAnulable() : boolean{
    if(((this.getOrdenServicio().estado.nombre).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  anularOrden() : void {
    if(!this.noAnulable()){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Cerrada?",
        onAceptar:()=>{
          this.confirmarAnularOrden();
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarAnularOrden() : void{
        this._servicioOrdenes.anularOrden(this.getOrdenServicio().codigo).subscribe(respuesta => {

          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          
          
          if(respuesta.objeto != null){
            this.setOrdenServicio(respuesta.objeto);
          }
          

          
          //Validar si se registró OK, mostrar form Registro Exitoso.
          if (respuesta.estado == "OK") {
            this.limpiarFormulario();
            this.enviado=true;
          }
        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Anular la Orden de Servicio con código N° '+this.getOrdenServicio().codigo+'. '+error
          });
          
        });
  }

  noCerrable() : boolean{
    if(((this.getOrdenServicio().estado.nombre).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  cerrarOrden() : void {
    if(!this.noCerrable()){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Cerrada?",
        onAceptar:()=>{
        	this.confirmarCerrarOrden();
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarCerrarOrden() : void {
      
        this._servicioOrdenes.cerrarOrden(this.getOrdenServicio()).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          
          if(respuesta.objeto != null){
            this.setOrdenServicio(respuesta.objeto);
          }
          
          //Validar si se registró OK, mostrar form Registro Exitoso.
          if (respuesta.estado == "OK") {
            this.limpiarFormulario();
            this.enviado=true;
          }
        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Cerrar la Orden de Servicio con código N° '+this.getOrdenServicio().codigo+'. '+error
          });
          
        });
      }
   

  redireccionConsultar() : void {
      this._router.navigate(['/ordenes-servicios/'+this.getOrdenServicio().codigo]);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';
import { Marcas } from 'src/app/modelos/marcas';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { MarcaService } from 'src/app/servicios/marca.service';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-nuevo-vehiculo',
  templateUrl: './nuevo-vehiculo.component.html',
  styleUrls: ['./nuevo-vehiculo.component.css']
})
export class NuevoVehiculoComponent implements OnInit {

  nuevoVehiculo : Vehiculos = new Vehiculos();
  private marcas : Marcas[] = [];
  enviado = false;

  constructor(private _servicioVehiculo : VehiculosService,
              private _servicioMarca : MarcaService,
              private url : UrlService) { }

  ngOnInit(): void {

    this._servicioMarca.getAllMarca().subscribe(respuesta => {
        this.setMarcas(respuesta);
      },error=>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
      });

  }

  @ViewChild('form') f : NgForm;

  cargarFormulario() {
    if (this.f.valid) {

      //Validación Tipo Propietario
      let prop = document.querySelector('input[name = "propietario"]:checked').getAttribute("value");
      switch (prop) {
        case "ANProp":
          this.nuevoVehiculo.razonSocialPropietario = null;
          break;
        case "RSProp":
          this.nuevoVehiculo.nombrePropietario = null;
          this.nuevoVehiculo.apellidoPropietario = null;
          break;
        default:
          AppComponent.confirmacion({
            titulo:"Error",
            mensaje:"No se ha seleccionado Tipo Propietario"
          });
      }

      //Validacion Dominio en Mayúsuculas
      this.nuevoVehiculo.dominio = this.nuevoVehiculo.dominio.toUpperCase();

      //Valor por defecto Chasis
      if (this.nuevoVehiculo.chasis == null) {
      this.nuevoVehiculo.chasis = "No especificado"
      }

      //Registro de Vehículo
      this._servicioVehiculo.guardarVehiculo(this.nuevoVehiculo).subscribe(respuesta => {

        let listaMsjs = "";
        for (let msj of respuesta.mensajes) {
          listaMsjs+=(msj.descripcion+"<br>");
        }

        AppComponent.confirmacion({
          titulo:"Resultado",
          mensaje: listaMsjs
        });

        //Validar si se registró OK, mostrar form Registro Exitoso.
        if (respuesta.estado == "OK") {
          this.enviado = true;
        }

      },
        error => {
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error",
            mensaje:"Ha ocurrido un ERROR en la Comunicación con el BACK: " + error
          });
      });
    } else {
      AppComponent.confirmacion({
        titulo:"Error",
        mensaje:'Hay campos incompletos o erróneos. Rellene todos los campos con los datos correctos'
      });
    }
  }

  limpiarFormulario() {
    this.f.reset();
  }

  setMarcas(valor) : void {
    this.marcas = valor;
  }

  getMarcas() : Marcas[] {
    return this.marcas;
  }

}

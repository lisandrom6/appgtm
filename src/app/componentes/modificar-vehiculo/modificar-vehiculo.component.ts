import { Component, OnInit, ViewChild } from '@angular/core';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { Marcas } from 'src/app/modelos/marcas';
import { MarcaService } from 'src/app/servicios/marca.service';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-modificar-vehiculo',
  templateUrl: './modificar-vehiculo.component.html',
  styleUrls: ['./modificar-vehiculo.component.css']
})
export class ModificarVehiculoComponent implements OnInit {

  private marcas : Marcas[] = [];
  dominio : string;
  vehiculoModificado : Vehiculos;
  enviado = false;


  constructor(private url : UrlService,private _activate : ActivatedRoute, private _servicioVehiculo : VehiculosService, private _servicioMarca : MarcaService) { }

  ngOnInit(): void {

    this._servicioMarca.getAllMarca().subscribe(respuesta => {
      this.setMarcas(respuesta);
    },
    error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    });

    this.vehiculoModificado = new Vehiculos();

    this.dominio=this._activate.snapshot.params['dominio'];
    this._servicioVehiculo.getVehiculos(this.dominio).subscribe(data => {
        this.vehiculoModificado = data.content[0];

        //Setear option Propietario
        if (this.vehiculoModificado.razonSocialPropietario == null) {
          document.querySelector("#radioNomApe").setAttribute('checked','true');
          document.querySelector("#content__datosPropietario-fieldset-nombreA").removeAttribute('hidden');
          document.querySelector("#ApeProp").setAttribute('required','true');
          document.querySelector("#NomProp").setAttribute('required','true');
        } else {
          document.querySelector("#radioRazonS").setAttribute('checked','true');
          document.querySelector("#content__datosPropietario-fieldset-razonS").removeAttribute('hidden');
          document.querySelector("#RazonSProp").setAttribute('required','true');
        }

      },
      error => {
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Ha ocurrido un ERROR en la Comunicación con el BACK: " + error
        });
  
       });

  }

  @ViewChild('form') f : NgForm;

  cargarFormulario(){
    if (this.f.valid) {

      //Validación Tipo Propietario
      let prop = document.querySelector('input[name = "propietario"]:checked').getAttribute("value");
      switch (prop) {
        case "ANProp":
          this.vehiculoModificado.razonSocialPropietario = null;
          break;
        case "RSProp":
          this.vehiculoModificado.nombrePropietario = null;
          this.vehiculoModificado.apellidoPropietario = null;
          break;
        default:       AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"No se ha seleccionado Tipo Propietario"
        });
      }

      //Validacion Dominio en Mayúsuculas
      this.vehiculoModificado.dominio = this.vehiculoModificado.dominio.toUpperCase();

      //Valor por defecto Chasis
      if (this.vehiculoModificado.chasis == null) {
        this.vehiculoModificado.chasis = "No especificado"
      }

      //Registro modificación de Vehículo
      this._servicioVehiculo.modificarVehiculo(this.vehiculoModificado).subscribe(respuesta => {

        let listaMsjs = "";
        for (let msj of respuesta.mensajes) {
          listaMsjs+=(msj.descripcion+"<br>");
        }

        AppComponent.confirmacion({
          titulo:"Resultado", 
          mensaje: listaMsjs
        });

        //Validar si se registró OK, mostrar form Registro Exitoso.
        if (respuesta.estado == "OK") {
          this.enviado = true;
        }

      },
        error => {
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:"Ha ocurrido un ERROR en la Comunicación con el BACK: " + error
          });
      });

    } else {
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:'Hay campos incompletos o erróneos. Rellene todos los campos con los datos correctos'
      });
      
    }
  }

  limpiarFormulario() {
    this.f.reset();
  }

  setMarcas(valor) : void {
    this.marcas = valor;
  }

  getMarcas() : Marcas[] {
    return this.marcas;
  }

  compareFn(a, b) {
    console.log('a= ', a, 'b= ', b, a.id == b?.id);
    return a.id == b?.id;
  }

}

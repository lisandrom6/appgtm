import { Component, OnInit } from '@angular/core';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { HttpResponse } from '@angular/common/http';
import { getLocaleDateFormat } from '@angular/common';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {

  vehiculos : Vehiculos[] = [];
  txtBuscar : string;
  filtrar = '';
  file: any = '';
  cantidadPaginas : number;
  paginaActual : number;

  constructor(private _servicioVehiculo : VehiculosService,
              private _router : Router,
              private url : UrlService) {

    this.listarTodos();
  }

  ngOnInit(): void {
    if(localStorage.getItem("vehiculo")){localStorage.removeItem("vehiculo");}
  }

  listarTodos(){
    this._servicioVehiculo.getAllVehiculos().subscribe(respuesta => {
      this.vehiculos = respuesta.content;

      this.cantidadPaginas = respuesta.totalPages
      this.paginaActual = respuesta.pageable.pageNumber;
    },error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    });

  }

  verDetallesVehiculo(valor : string) {
    this._router.navigate(['/vehiculos', valor]);
  }

  verOSPorVehiculo(dominio) {
    this._router.navigate(['/ordenes-servicios/listar', dominio]);
  }

  nuevaOrdenServicio(dominio : string) {
    this.setStorageVehiculo(dominio);
    this._router.navigate(['/ordenes-servicios/new']);
  }

  setStorageVehiculo(dominio: string) {
    localStorage.removeItem("vehiculo");
    localStorage.setItem("vehiculo", dominio);
  }

  buscarVehiculo() {
    if(this.txtBuscar != ''){
      this._servicioVehiculo.getVehiculos(this.txtBuscar).subscribe(respuesta =>{
        if (respuesta.content[0] == null) {
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:"No se encontraron resultados para el valor ingresado. Intente ingresando otro valor, registre un nuevo vehículo o diríjase a la sección Vehículos"
          });
          this.limpiarBusqueda();
        } else {
          this.vehiculos = respuesta.content;
        }
        this.txtBuscar='';
        localStorage.removeItem("vehiculo");
      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'Ha ocurrido un error al buscar el Vehículo con dominio: ' + this.txtBuscar + '. ' + error
        });
      });
    } else {
      AppComponent.confirmacion({
        titulo:"Información", 
        mensaje:"Ingrese un dominio a buscar"
      });

      this.txtBuscar='';
    }
  }

  /* buscarVehiculo() {
    this._servicioVehiculo.getVehiculos(this.txtBuscar).subscribe(respuesta => {
      console.log(respuesta.content[0]);
      if (respuesta.content[0] == null) {
        alert("No se encontraron resultados para el valor ingresado. Intente ingresando otro valor o Registre un nuevo vehículo");
        this.limpiarBusqueda();
      } else {
        this.vehiculos = respuesta.content[0];
      }
    });
  } */

  verNuevoVehiculo() {
    if(localStorage.getItem("vehiculo")){localStorage.removeItem("vehiculo");}
    this._router.navigate(['/vehiculos/new']);
  }

  limpiarBusqueda() {
    this.txtBuscar = '';
    this.listarTodos();
  }

  getFileName(response: HttpResponse<Blob>) {
    let filename: string;
    try {
      const contentDisposition: string = response.headers.get('content-disposition');
      const r = /(?:filename=")(.+)(?:")/
      filename = r.exec(contentDisposition)[1];
    }
    catch (e) {
      let fecha : Date = new Date();
      let fechaPdf : String = '' + fecha.getFullYear() + (fecha.getMonth()+1) + fecha.getDate();
      filename = 'ListadoVehículos' + fechaPdf + '.pdf';
    }
    return filename
  }


  downloadFile() {
    this._servicioVehiculo.downloadFile()
      .subscribe(
        (response: HttpResponse<Blob>) => {
          let filename: string = this.getFileName(response)
          let binaryData = [];
          binaryData.push(response.body);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: 'blob' }));
          downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error=>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
        }
      )
  }

  pasarPagina(valor : number) {
    this.paginaActual+=valor;
    this._servicioVehiculo.getAllVehiculos(this.paginaActual).subscribe(respuesta => {
      this.vehiculos = respuesta.content;
    },error=>{
      console.log("--------------------------------------------")
      console.log(error)
      console.log("--------------------------------------------")
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    })
  }

  irAPagina(valor : number){
    this._servicioVehiculo.getAllVehiculos(valor).subscribe(respuesta => {
      this.vehiculos = respuesta.content;
      this.paginaActual=valor;
    },error=>{
      if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
        window.location.href = this.url.getURLAth();
      }
    })
  }

}

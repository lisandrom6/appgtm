import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

import { OrdenServicioService } from 'src/app/servicios/orden-servicio.service';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';

import { DetalleOrden } from 'src/app/modelos/detallesOrden';
import { OrdenServicio } from 'src/app/modelos/ordenesServicio';
import { Vehiculos } from 'src/app/modelos/vehiculos';
import { Marcas } from 'src/app/modelos/marcas';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';


@Component({
  selector: 'app-nueva-orden-servicio',
  templateUrl: './nueva-orden-servicio.component.html',
  styleUrls: ['./nueva-orden-servicio.component.css']
})
export class NuevaOrdenServicioComponent implements OnInit {

  detalles : DetalleOrden[] = [{id: 1, descripcion: "", subtotal:0, nueva: true}];
  ordenServicio : OrdenServicio;
  txtBuscar : string ="";
  seleccionado : string;
  enviado : boolean = false;
  hayVehiculo : boolean;

  constructor(private _servicioOrden    : OrdenServicioService,
              private _servicioVehiculo : VehiculosService,
              private _formatFecha      : DatePipe,
              private _router           : Router,
              private url : UrlService) {
    this.inicializador();
  }

  inicializador() {
    this.ordenServicio = new OrdenServicio();
    this.detalles = [{id: 1, descripcion: "", subtotal:0, nueva: true}];
    this.ordenServicio.detalle = this.detalles;
    this.ordenServicio.vehiculo = new Vehiculos();
    this.ordenServicio.vehiculo.marca = new Marcas()
    this.ngOnInit();
  }

  ngOnInit(): void {
    if(localStorage.getItem("vehiculo")){             //Valido si en el Home/Listado de Vehículos/Detalle he buscado un Vehículo.
      this.txtBuscar=localStorage.getItem("vehiculo");
      this.buscarVehiculo();
    }

    this.ordenServicio.fecha = this.setDate();
    this.ordenServicio.abonada= false;
  }
  
  buscarVehiculo() {
    if(this.txtBuscar && this.validarInputVacio(this.txtBuscar) ){
      if((localStorage.getItem("vehiculo"))){
        localStorage.removeItem("vehiculo");
      }else{
        this.limpiarFormulario(this.f)
      }
      this._servicioVehiculo.consultarVehiculo(this.txtBuscar).subscribe(respuesta =>{
        if (respuesta.content[0] == null) {
          this.hayVehiculo = false;
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:"No se encontraron resultados para el valor ingresado. Intente ingresando otro valor, registre un nuevo vehículo o diríjase a la sección Vehículos"
          });
        } else {
          this.hayVehiculo = true;
          this.setVehiculo(respuesta.content[0]);
        }
        this.txtBuscar='';
        localStorage.removeItem("vehiculo");
      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'Ha ocurrido un error al buscar el Vehículo con dominio: ' + this.txtBuscar + '. ' + error
        });
      });
    } else {
      AppComponent.confirmacion({
        titulo:"Información", 
        mensaje:"Ingrese el dominio a buscar"
      });
      this.txtBuscar='';
    }
  }

  verNuevoVehiculo() {
    this._router.navigate(['/vehiculos/new']);
  }

  getVehiculo() {
    return this.getOrdenServicio().vehiculo;
  }

  setVehiculo(valor) {
    this.ordenServicio.vehiculo = valor;
    this.setPropietario();
    this.ordenServicio.kilometrajeActual = valor.kilometraje;
  }

  setPropietario() : void {
    let fieldset;           //Las dos variables se crean sólo por legibilidad del código, para no tener que validar en el HTML qué objetos son.
    let input;
    if(!((this.getOrdenServicio().vehiculo.razonSocialPropietario == null) && (this.getOrdenServicio().vehiculo.nombrePropietario == null))) {
      if ((this.getOrdenServicio().vehiculo.razonSocialPropietario == null) || (this.getOrdenServicio().vehiculo.razonSocialPropietario.length <= 0 )){
        this.getOrdenServicio().nombreTitular = this.getOrdenServicio().vehiculo.nombrePropietario;
        this.getOrdenServicio().apellidoTitular = this.getOrdenServicio().vehiculo.apellidoPropietario;
        this.getOrdenServicio().telefonoTitular = this.getOrdenServicio().vehiculo.telefonoPropietario;
        fieldset = document.querySelector("#content__datosPropietario-fieldset-nombreA").removeAttribute('hidden');
        input = document.querySelector("#nombrePropietario").setAttribute('required','true');

      } else if ((this.getOrdenServicio().vehiculo.nombrePropietario == null)||(this.getOrdenServicio().vehiculo.nombrePropietario.length <= 0 )){
        this.getOrdenServicio().razonSocialTitular = this.getOrdenServicio().vehiculo.razonSocialPropietario;
        this.getOrdenServicio().telefonoTitular = this.getOrdenServicio().vehiculo.telefonoPropietario;
        fieldset = document.querySelector("#content__datosPropietario-fieldset-razonS").removeAttribute('hidden');
        input = document.querySelector("#razonSocialPropietario").setAttribute('required','true');

      } else {
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Error: No se puede cargar el propietario del vehículo en cuestión. /n Contáctese con su proveedor del sistema. /n Lamentamos las molestias ocasionadas."
        });
      }
    } else {
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"Error: No se puede cargar el propietario del vehículo en cuestión. /n Contáctese con su proveedor del sistema. /n Lamentamos las molestias ocasionadas."
      });
    }
  }

  getOrdenServicio() : OrdenServicio {
    return this.ordenServicio;
  }

  setOrdenServicio(orden) : void {
    this.ordenServicio = orden;
  }

  setDate(fecha : any  = Date.now()) : string {
    let ahora : number = fecha;
    return this._formatFecha.transform(ahora,'dd/MM/yyyy');
  }

  setTotal(){
    let total : number = 0;
    this.detalles.forEach(element => {
      total += element.subtotal;
    });
    this.getOrdenServicio().total = total;
  }

  agregarTrabajo(){
    let ind = this.getOrdenServicio().detalle.length;
    console.log(ind);
    let det : DetalleOrden = {id:ind+1,descripcion:'',subtotal:0, nueva:true}
    console.log("+"+det);
    this.getOrdenServicio().detalle.push(det);
  }

  quitarTrabajo(indice : number) :void {
    if (!(this.getOrdenServicio().detalle[indice-1].subtotal == null || this.getOrdenServicio().total<=0)) {
      this.getOrdenServicio().total -= this.detalles[indice-1].subtotal;
    }
    this.getOrdenServicio().detalle.splice(indice-1,1);
    this.getOrdenServicio().detalle.forEach(element => {
      if(element.id>indice){
        element.id-=1;
      }
    });
  }

  @ViewChild('form') f : NgForm;
  
  limpiarFormulario(form : NgForm = this.f) : void{
    if(form) {form.reset()}
    this.enviado = false;
    this.hayVehiculo = false;
    document.querySelector("#content__datosPropietario-fieldset-nombreA").setAttribute('hidden', 'true');
    document.querySelector("#nombrePropietario").setAttribute('required','false');
    document.querySelector("#content__datosPropietario-fieldset-razonS").setAttribute('hidden', 'true');
    document.querySelector("#razonSocialPropietario").setAttribute('required','false');
  }

  // hayTotal() : boolean {
  //   let bool : boolean = false;
  //   if(this.getOrdenServicio().total>0){
  //     let slider = document.querySelector(".abonada");
  //     slider.setAttribute('disabled','false')
  //     console.log(slider);
  //     bool = true;
  //   }
  //   return bool
  // }

  setNivelCombustible() : void {
    var combustible = document.querySelector('input[name = "combustible"]:checked').getAttribute("value");
    switch (combustible) {
      case "vacio": this.getOrdenServicio().nivelCombustible=0;
        break;
      case "cuarto": this.getOrdenServicio().nivelCombustible=0.25;
        break;
      case "medio": this.getOrdenServicio().nivelCombustible=0.5;
        break;
      case "tres-cuartos": this.getOrdenServicio().nivelCombustible=0.75;
        break;
      case "lleno": this.getOrdenServicio().nivelCombustible=1;
        break;
      default: 
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:"El nivel de combustible debe ser cargado"
      });
    }

  }

  setEnviado() : void {
    this.inicializador();
    this.limpiarFormulario();
  }

  setFecha() : void {
    let fec = Date.now();
    this.getOrdenServicio().fecha = this._formatFecha.transform(fec,'yyyy-MM-dd');
    if(this.getOrdenServicio().fechaRecepcion){
      this.getOrdenServicio().fechaRecepcion = this._formatFecha.transform(this.getOrdenServicio().fechaRecepcion,'yyyy-MM-dd');
    }
  }

  seteoOrden() : void {
    this.setNivelCombustible();
    this.setFecha();
    this.setDetalles();
    if(this.getOrdenServicio().abonada){
      this.getOrdenServicio().estado = {id: 4 , nombre:'Abonada',descripcion:''};
    }else{
          this.getOrdenServicio().estado = {id: 1 , nombre:'Abierta',descripcion:''};
    }
  }

  seteoVehiculo() : void {
    this.getOrdenServicio().vehiculo.kilometraje=this.getOrdenServicio().kilometrajeActual;
    this.getOrdenServicio().vehiculo.nombrePropietario=this.getOrdenServicio().nombreTitular;
    this.getOrdenServicio().vehiculo.apellidoPropietario=this.getOrdenServicio().apellidoTitular;
    this.getOrdenServicio().vehiculo.razonSocialPropietario=this.getOrdenServicio().razonSocialTitular;
    this.getOrdenServicio().vehiculo.telefonoPropietario=this.getOrdenServicio().telefonoTitular;
  }

  checkCheckBoxvalue(event){
    this.getOrdenServicio().abonada = !(this.getOrdenServicio().abonada);
  }

  validarInputVacio(input : string) {
    let value = input.trim().length;
    if (value > 0) {
      return true;
    } else {
      return false;
    };
  }

  guardarOrden(form : NgForm) : void {
    if (form.valid) {
      if(((this.getOrdenServicio().razonSocialTitular != null) || (this.getOrdenServicio().nombreTitular != null))) {
        this.seteoOrden();
        this.seteoVehiculo();
        this._servicioOrden.guardarOrden(this.getOrdenServicio()).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });


          if(respuesta.objeto != null){
            this.setOrdenServicio(respuesta.objeto);
          }

          //Validar si se registró OK, mostrar form Registro Exitoso.
          if (respuesta.estado == "OK") {
            this.limpiarFormulario(form);
            this.enviado = true;
          }
        },
          error => {
            if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
              window.location.href = this.url.getURLAth();
            }
            AppComponent.confirmacion({
              titulo:"Error", 
              mensaje:"Ha ocurrido un ERROR en la Comunicación: " + error
            });
        });

      } else {
        console.log(this.getOrdenServicio());
          
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Debe definir un Nombre-Apellido o Razón Social para el Propietario del Vehículo."
        });
        
      }
    } else {
      AppComponent.confirmacion({
        titulo:"Error", 
        mensaje:'Hay campos incompletos o erróneos. Rellene todos los campos con los datos correctos.'
      });
    }
  }

  setDetalles() {
    this.getOrdenServicio().detalle.forEach(element => {
        element.id=0;
    });
  }

  redireccionConsultar() : void {
    this._router.navigate(['/ordenes-servicios/'+this.getOrdenServicio().codigo]);
  }
}

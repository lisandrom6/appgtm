import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TOKEN_KEY } from 'src/app/app.component';
import { UrlService } from 'src/app/servicios/url.service';
import { VehiculosService } from 'src/app/servicios/vehiculos.service';
import { JWTTokenService } from 'src/JWTTokenService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  presupuesto : boolean = false; //No se encuentra realizado en este release;

  constructor(private _router           : Router, 
              private _servicioVehiculo : VehiculosService,
              private url : UrlService,
              private jwtService:JWTTokenService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void { 
    if(localStorage.getItem("vehiculo")){localStorage.removeItem("vehiculo");}
    let divVehiculo = document.querySelector("#divVehiculo");
    divVehiculo.className="container-fluid";


    if(this.url.getURLAth()){
      let jwt = window.sessionStorage.getItem(TOKEN_KEY)
      if(jwt){
        this.jwtService.setToken(jwt)
      }
    }
  }

  verNuevoVehiculo() {
    if(localStorage.getItem("vehiculo")){localStorage.removeItem("vehiculo");}
    this._router.navigate(['/vehiculos/new']);
  }
  
  verNuevaOrdenServicio() { 
    this._router.navigate(['/ordenes-servicios/new']);
  }

  verNuevoPresupuesto() {
    this._router.navigate(['/presupuestos/new']);
  }

  getImagen(){
    return this.jwtService.getEntornoImagen()?this.jwtService.getEntornoImagen():"assets/imagenes/logo.png"
  }
}


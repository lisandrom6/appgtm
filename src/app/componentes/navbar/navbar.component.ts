import { Component, OnInit } from '@angular/core';
import { TOKEN_KEY } from 'src/app/app.component';
import { UrlService } from 'src/app/servicios/url.service';
import { JWTTokenService } from 'src/JWTTokenService';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  validaSesion : boolean;
  presupuesto : boolean;
  constructor(private url : UrlService,private jwtService:JWTTokenService) { 
    this.presupuesto=false; //No está realizado en este realease.
  }

  ngOnInit(): void {
    if(this.url.getURLAth()){
      this.validaSesion = true
      let jwt = window.sessionStorage.getItem(TOKEN_KEY)
      if(jwt){
        this.jwtService.setToken(jwt)
      }
    }
  }
  getNombreUsuario(){
    return this.jwtService.getEntornoNombre()+"("+this.jwtService.getSub()+")"
  }
  getImagen(){
    return this.jwtService.getEntornoImagen()?this.jwtService.getEntornoImagen():"assets/imagenes/logo.png"
  }
  modulos(){
    window.location.href = this.url.getURLAth()
  }
  cerrarSesion(){
    window.sessionStorage.clear()
    window.location.href = this.url.getURLAth()
  }
}

import { Component, OnInit } from '@angular/core';
import { OrdenServicioService } from 'src/app/servicios/orden-servicio.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OrdenServicio } from 'src/app/modelos/ordenesServicio';
import { Estado } from 'src/app/modelos/estados';
import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { UrlService } from 'src/app/servicios/url.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-orden-servicio',
  templateUrl: './orden-servicio.component.html',
  styleUrls: ['./orden-servicio.component.css']
})
export class OrdenServicioComponent implements OnInit {

  private ordenServicio : OrdenServicio = new OrdenServicio();
  txtBuscar : string = '';

  constructor(private _servicioOrdenes : OrdenServicioService,
              private _activate        : ActivatedRoute,
              private _formatFecha     : DatePipe,
              private _router          : Router,
              private url : UrlService) {
    this._activate.params.subscribe((parametros) =>{
      this._servicioOrdenes.getOrden(parametros['nro-orden']).subscribe(respuesta =>{
        this.setOrden(respuesta.content[0]);
      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:'No se ha encontrado la Orden de Servicio con código N° '+parametros['nro-orden']+'. '+error
        });
      });
    });
  }

  ngOnInit(): void {
  }

  setOrden(orden) : void {
    this.ordenServicio = orden;
  }

  buscarOrden(){
    if (this.txtBuscar != ''){
      this._servicioOrdenes.getOrden(this.txtBuscar).subscribe(respuesta =>{
        this.setOrden(respuesta.content[0]);
        console.log(respuesta.content[0]);
        if (respuesta.content[0] == undefined) {
          alert ("No se encontraron resultados");
        }
      },
      error => {
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        AppComponent.confirmacion({
          titulo:"Error", 
          mensaje:"Hubo un inconveniente al intentar obtener la Orden de Servicio N° "+this.txtBuscar+". "+ error
        });
      });
      this.txtBuscar='';
    }
    else {
      AppComponent.confirmacion({
        titulo:"Información", 
        mensaje:"Campo vacío. Ingrese un valor para continuar",
        onAceptar:()=>{}
      });
    }
  }

/*   buscarOrden() : void{
    if (this.validarInputVacio(this.txtBuscar) == true) {
      this._servicioOrdenes.getOrden(this.txtBuscar).subscribe(respuesta => {
        this.setOrden(respuesta);
      },
      error =>{
        if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
          window.location.href = this.url.getURLAth();
        }
        alert('No se ha encontrado la Orden de Servicio con código N° '+this.txtBuscar+'. '+error);
      });
    }
  }

  validarInputVacio(input : string) : boolean{
    let value = input.trim().length;
    if (value === 0) {
      alert ("Campo vacío. Ingrese un valor para continuar");
      return false;
    } else {
      return true;
    };
  } */

  getFecha() : string {
    return this.setDate(this.getOrden().fecha);
  }

  getFechaRecepcion() : string {
    return this.setDate(this.getOrden().fechaRecepcion);
  }

  setDate(fecha : any ) : string {
    let ahora : number = fecha;
    return this._formatFecha.transform(ahora,'dd/MM/yyyy');
  }

  limpiarBusqueda() : void{
    this.ordenServicio = undefined;
    this.txtBuscar='';
  }

  getOrden(){
    return this.ordenServicio;
  }
  getKilometrajeActual() : string {
    let kilometraje : string;
    switch (this.getOrden().nivelCombustible) {
      case 0: kilometraje = 'Vacío';
        break;
      case 0.25: kilometraje = '1/4';
        break;
      case 0.5: kilometraje = '1/2';
        break;
      case 0.75: kilometraje = '3/4';
        break;
      case 1: kilometraje = 'Lleno';
        break;
      default: kilometraje = 'Sin Datos';
    }
    return kilometraje;
  }

  modificarOrden(codigo : number) {
    this._router.navigate(['/ordenes-servicios/update/', codigo]);
  }

  noAnulable() : boolean{
    if(((this.getOrden().estado.nombre).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  anularOrden() : void {
    if(!this.noAnulable()){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Anulada?",
        onAceptar:()=>{
        	this.confirmarAnularOrden()
        },
        onCancelar:()=>{}
      });
    }
  }
      
  confirmarAnularOrden() : void {        
        this._servicioOrdenes.anularOrden(this.getOrden().codigo).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });

          if(respuesta.objeto != null){
            this.setOrden(respuesta.objeto);
          }



        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Anular la Orden de Servicio con código N° '+this.getOrden().codigo+'. '+error
          });
        });
      }
    
  

  noAbonable() : boolean{
    if(((this.getOrden().estado.nombre).indexOf("Abonada"))>=0 || ((this.getOrden().estado.nombre).indexOf("Anulada"))>=0) {
      return true;
    } else {
      return false;
    }
  }

  abonarOrden(){
    if(!this.noAbonable()){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Abonada?",
        onAceptar:()=>{
        	this.confirmarAbonarOrden()
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarAbonarOrden(){
        this._servicioOrdenes.abonarOrden(this.getOrden()).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          if(respuesta.objeto != null){
            this.setOrden(respuesta.objeto);
          }

        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Abonar la Orden de Servicio con código N° '+this.getOrden().codigo+'. '+error
          });
        });
      }


  noCerrable() : boolean{
    if(((this.getOrden().estado.nombre).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  cerrarOrden() : void {
    if(!this.noCerrable()){
      AppComponent.confirmacion({
        titulo:"Confirmación", 
        mensaje:"Está seguro que desea marcar esta Orden de Servicio como Cerrada?",
        onAceptar:()=>{
        	this.confirmarCerrarOrden()
        },
        onCancelar:()=>{}
      });
    }
  }
  confirmarCerrarOrden():void{ 
      
        this._servicioOrdenes.cerrarOrden(this.getOrden()).subscribe(respuesta => {
          let listaMsjs = "";
          for (let msj of respuesta.mensajes) {
            listaMsjs+=(msj.descripcion+"<br>");
          }
  
          AppComponent.confirmacion({
            titulo:"Resultado", 
            mensaje: listaMsjs
          });
          if(respuesta.objeto != null){
            this.setOrden(respuesta.objeto);
          }


        },
        error =>{
          if((error.status==401 || error.status==403 || error.status==0) && this.url.getURLAth()){
            window.location.href = this.url.getURLAth();
          }
          AppComponent.confirmacion({
            titulo:"Error", 
            mensaje:'No se ha podido Cerrar la Orden de Servicio con código N° '+this.getOrden().codigo+'. '+error
          });
        });
      }


  modificable() : boolean {
    if(((this.getOrden().estado.nombre).indexOf("Abierta"))!=0){
      return true;
    } else {
      return false;
    }
  }

  getFileName(response: HttpResponse<Blob>) {
    let filename: string;
    try {
      const contentDisposition: string = response.headers.get('content-disposition');
      const r = /(?:filename=")(.+)(?:")/
      filename = r.exec(contentDisposition)[1];
    }
    catch (e) {
      let nroOrden : number = this.getOrden().codigo;
      filename = 'Orden' + nroOrden + '.pdf';
    }
    return filename
  }

  descargarOS() {
    this._servicioOrdenes.downloadFile(this.getOrden().codigo)
      .subscribe(
        (response: HttpResponse<Blob>) => {
          let filename: string = this.getFileName(response)
          let binaryData = [];
          binaryData.push(response.body);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: 'blob' }));
          downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        }
      )
  }
}

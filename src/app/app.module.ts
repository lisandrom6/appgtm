import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { FilterPipe } from './pipes/filter.pipe';

import { VehiculosService } from './servicios/vehiculos.service';
import { UrlService } from './servicios/url.service';
import { MarcaService } from './servicios/marca.service';
import { OrdenServicioService } from './servicios/orden-servicio.service';
import { PresupuestoService } from './servicios/presupuesto.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { VehiculoComponent } from './componentes/vehiculo/vehiculo.component';
import { VehiculosComponent } from './componentes/vehiculos/vehiculos.component';
import { NuevoVehiculoComponent } from './componentes/nuevo-vehiculo/nuevo-vehiculo.component';

import { OrdenServicioComponent } from './componentes/orden-servicio/orden-servicio.component';
import { OrdenesServiciosComponent } from './componentes/ordenes-servicios/ordenes-servicios.component';
import { NuevaOrdenServicioComponent } from './componentes/nueva-orden-servicio/nueva-orden-servicio.component';
import { ModificarOrdenServicioComponent } from './componentes/modificar-orden-servicio/modificar-orden-servicio.component';
import { PresupuestoComponent } from './componentes/presupuesto/presupuesto.component';
import { PresupuestosComponent } from './componentes/presupuestos/presupuestos.component';
import { NuevoPresupuestoComponent } from './componentes/nuevo-presupuesto/nuevo-presupuesto.component';
import { ModificarPresupuestoComponent } from './componentes/modificar-presupuesto/modificar-presupuesto.component';
import { ModificarVehiculoComponent } from './componentes/modificar-vehiculo/modificar-vehiculo.component';
import { OsPorVehiculoComponent } from './componentes/os-por-vehiculo/os-por-vehiculo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// //PDF MAKER WRAPPER
// // Import pdfmake-wrapper and the fonts to use
// import { PdfMakeWrapper } from 'pdfmake-wrapper';
// import pdfFonts from "pdfmake/build/vfs_fonts";
// // fonts provided for pdfmake
// // Set the fonts to use
// PdfMakeWrapper.setFonts(pdfFonts);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    VehiculosComponent,
    VehiculoComponent,
    NuevoVehiculoComponent,
    ModificarVehiculoComponent,
    FilterPipe,
    OrdenServicioComponent,
    OrdenesServiciosComponent,
    NuevaOrdenServicioComponent,
    ModificarOrdenServicioComponent,
    PresupuestoComponent,
    PresupuestosComponent,
    NuevoPresupuestoComponent,
    ModificarPresupuestoComponent,
    OsPorVehiculoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [
    VehiculosService,
    MarcaService,
    OrdenServicioService,
    PresupuestoService,
    UrlService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

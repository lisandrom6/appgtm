import { Pipe, PipeTransform } from '@angular/core';
import { OrdenServicioService } from '../servicios/orden-servicio.service';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  
  transform(elements: any, busqueda: any): any {

    var pathname = window.location.pathname;
    const resultado = [];

    if (busqueda == ' ' || busqueda == '  ' || busqueda == '   ') return elements;
    if (pathname.indexOf('/vehiculos') != -1){
      for (const e of elements) {
        //Utilizado en Vehículos
        if ((e.apellidoPropietario?.toLowerCase().indexOf(busqueda.toLowerCase()) != -1) ||
            (e.nombrePropietario?.toLowerCase().indexOf(busqueda.toLowerCase()) != -1) ||
            (e.razonSocialPropietario?.toLowerCase().indexOf(busqueda.toLowerCase()) != -1)) {
          resultado.push(e);
        };
      };
    } else if (pathname.indexOf('/ordenes-servicios') != -1){
      //Utilizado en Ordenes de Servicio
      for (const e of elements) {
        if (e.vehiculo.dominio?.toLowerCase().indexOf(busqueda.toLowerCase()) != -1) {
          resultado.push(e);
        }
      };
    }
    return resultado;
  }

}

import { Vehiculos } from './vehiculos';
import { DetalleOrden } from './detallesOrden';
import { Estado } from './estados';

export class OrdenServicio {
  codigo : number;
  nombreTitular : string;
  apellidoTitular : string;
  razonSocialTitular : string;
  telefonoTitular : number;
  nombreChofer : string;
  apellidoChofer : string;
  telefonoChofer : number;
  vehiculo : Vehiculos = new Vehiculos();
  detalle : DetalleOrden[]; 
	estado : Estado;
	fecha : any;
	total : number;
  nivelCombustible : number;
  fechaRecepcion : any;
  horaRecepcion : any;
	kilometrajeActual : number;
  motivoVisita : string;
  abonada : boolean
}
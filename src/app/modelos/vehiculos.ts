import { Marcas } from './marcas';

export class Vehiculos{
  id : number;
  dominio: string;
  marca: Marcas;
  modelo: string;
  chasis: string;
  anioModelo: number;
  color: string;
  kilometraje: number;
  nombrePropietario: string;
  apellidoPropietario: string;
  razonSocialPropietario: string;
  telefonoPropietario: number;
}

